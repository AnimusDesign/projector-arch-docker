#!/usr/bin/env bash
docker run --rm   \
    -p 9887:8887 \
    -p 9880:8880 \
	-v /DockerData/projector/intellij-personal:/home/developer \
	--name arch-webstorm \
	-e ORG_JETBRAINS_PROJECTOR_SERVER_SSL_PROPERTIES_PATH=/home/developer/ssl.properties \
	-e WEB_PORT=8880 \
	arch-webstorm
