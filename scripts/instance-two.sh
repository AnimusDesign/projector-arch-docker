#!/usr/bin/env bash
docker run --rm  -d \
    -p 8888:8887 \
    -p 8890:8880 \
	-v /DockerData/projector/intellij-personal:/home/developer \
	--name arch-idea2 \
	-e ORG_JETBRAINS_PROJECTOR_SERVER_SSL_PROPERTIES_PATH=/home/developer/ssl.properties \
	-e WEB_PORT=8890 \
	arch-idea
