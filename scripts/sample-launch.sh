#!/usr/bin/env bash
docker run --rm -d \
    -p 8887:8887 \
    -p 8880:8880 \
	-v /DockerData/projector/intellij-personal:/home/developer \
	-v /var/run/docker.sock:/var/run/docker.sock \
	--name arch-idea \
	-e ORG_JETBRAINS_PROJECTOR_SERVER_SSL_PROPERTIES_PATH=/home/developer/ssl.properties \
	-e WEB_PORT=8880 \
	arch-idea
