#
# Copyright 2019-2020 JetBrains s.r.o.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

FROM debian AS ideDownloader

# prepare tools:
RUN apt-get update
RUN apt-get install wget -y
# download IDE to the /ide dir:
WORKDIR /download
ARG downloadUrl
RUN wget -q $downloadUrl -O - | tar -xz
RUN find . -maxdepth 1 -type d -name * -execdir mv {} /ide \;

FROM amazoncorretto:11 as projectorGradleBuilder

ENV PROJECTOR_DIR /projector

# projector-server:
ADD projector-server $PROJECTOR_DIR/projector-server
WORKDIR $PROJECTOR_DIR/projector-server
ARG buildGradle
RUN if [ "$buildGradle" = "true" ]; then ./gradlew clean; else echo "Skipping gradle build"; fi
RUN if [ "$buildGradle" = "true" ]; then ./gradlew :projector-server:distZip; else echo "Skipping gradle build"; fi

FROM debian AS projectorStaticFiles

# prepare tools:
RUN apt-get update
RUN apt-get install unzip -y
# create the Projector dir:
ENV PROJECTOR_DIR /projector
RUN mkdir -p $PROJECTOR_DIR
# copy IDE:
COPY --from=ideDownloader /ide $PROJECTOR_DIR/ide
# copy projector files to the container:
ADD projector-arch-docker/static $PROJECTOR_DIR
# copy projector:
COPY --from=projectorGradleBuilder $PROJECTOR_DIR/projector-server/projector-server/build/distributions/projector-server-1.0-SNAPSHOT.zip $PROJECTOR_DIR
# prepare IDE - apply projector-server:
RUN unzip $PROJECTOR_DIR/projector-server-1.0-SNAPSHOT.zip
RUN rm $PROJECTOR_DIR/projector-server-1.0-SNAPSHOT.zip
RUN mv projector-server-1.0-SNAPSHOT $PROJECTOR_DIR/ide/projector-server
RUN mv $PROJECTOR_DIR/ide-projector-launcher.sh $PROJECTOR_DIR/ide/bin
RUN chmod 644 $PROJECTOR_DIR/ide/projector-server/lib/*

FROM archlinux:base-devel

RUN useradd developer
RUN mkdir -p /home/developer

RUN pacman -Suyy --noconfirm
RUN pacman -S --noconfirm vim neofetch zsh git go curl wget tree libsecret gnome-keyring sudo
RUN pacman -S --noconfirm p7zip unzip
RUN pacman -S --noconfirm python python-pip
RUN pacman -S --noconfirm kubectl kubectx k9s docker
RUN pacman -S --noconfirm terraform
RUN pacman -S --noconfirm libxext libxrender libxtst freetype2 libxi  
RUN pacman -S --noconfirm jdk11-openjdk
RUN pacman -S --noconfirm maven gradle bazel
RUN pacman -S --noconfirm nodejs-lts-erbium npm yarn deno

RUN npm install -g typescript eslint
RUN npm install -g aws-cdk cdk8s-cli 
RUN npm install -g cdktf-cli@next 
ARG downloadUrl

# copy the Projector dir:
ENV PROJECTOR_DIR /projector
COPY --from=projectorStaticFiles $PROJECTOR_DIR $PROJECTOR_DIR

RUN true \
# Any command which returns non-zero exit code will cause this shell script to exit immediately:
    && set -e \
# Activate debugging to show execution details: all commands will be printed before execution
    && set -x \
# move run scipt:
    && mv $PROJECTOR_DIR/run.sh run.sh \
# change user to non-root (http://pjdietz.com/2016/08/28/nginx-in-docker-without-root.html):
    && chown -R developer:developer /home/developer \
    && chown -R developer:developer $PROJECTOR_DIR/ide/bin \
    && chown developer:developer run.sh

RUN chsh -s /usr/bin/zsh developer
RUN chown -Rc developer:developer /home/developer
USER developer
RUN pip3 install projector-installer --user 

WORKDIR /tmp

RUN wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
RUN tar -xvf yay.tar.gz
RUN cd yay; makepkg; 

USER root
RUN echo "%wheel ALL = (ALL) NOPASSWD: ALL" > /etc/sudoers
RUN chsh -s  /usr/bin/zsh  developer
RUN gpasswd -a developer wheel
RUN pacman --noconfirm -U /tmp/yay/yay*.tar.zst
RUN rm -fvr /tmy/yay*
RUN archlinux-java set java-11-openjdk
RUN mkdir -p /skel; \
	mv -fv /home/developer /skel; \
	chown -Rc developer:developer /skel/developer; \
    mkdir - /home/developer; \
	chown -Rc developer:developer /home/developer;
RUN wget https://github.com/holgerbrandl/kscript/releases/download/v3.0.2/kscript-3.0.2-bin.zip; \
	unzip kscript-3.0.2-bin.zip;  \
    mv -fv kscript-3.0.2/bin/* /usr/local/bin; \
    rm -fvr kscript-3.0.2; \
    chmod +x /usr/local/bin/*;

USER developer
RUN yes |yay -S --noconfirm ncurses5-compat-libs;
RUN yes |yay -S --noconfirm native-image-jdk11-bin jdk11-graalvm-bin kotlin
RUN yes |yay -S --noconfirm ki-shell-bin


USER root
RUN pacman -Rc --noconfirm sudo

USER developer
WORKDIR /home/developer
CMD ["bash", "-c", "/run.sh"]
